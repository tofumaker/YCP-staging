from django.conf.urls import include, url
from django.contrib import admin
from home.views import Home

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^$', Home),
]
