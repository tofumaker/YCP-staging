from django.db import models

class CommonSection(models.Model):
    title = models.CharField(max_length=255)
    sub_title = models.CharField(blank=True, null=True, max_length=255)
    text = models.TextField(blank=True, null=True)

    class Meta:
        abstract = True

class Section(CommonSection):
    section_name = models.CharField(max_length=255)

    def __str__(self):
        return self.section_name

    class Meta:
        verbose_name = "Section"
        verbose_name_plural = "Sections"
